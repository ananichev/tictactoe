# TicTacToe #

Tic-tac-toe (also known as noughts and crosses or Xs and Os) is a paper-and-pencil game for two players, X and O, who take turns marking the spaces in a 3×3 grid. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the game.

### To start game: ###

* clone `tictactoe` file to your PC
* run `./path/to/file/tictactoe` in terminal 

After the game starts, players should see a 3x3 grid. Then they should type numbers from 1 to 9 corresponding to the cells indexes.
```
     |     | 
  1  |  2  |  3   
-----------------
     |     | 
  4  |  5  |  6   
-----------------
     |     | 
  7  |  8  |  9  
```

 To restart the game - type `R`

 To quit the game - type `Q`